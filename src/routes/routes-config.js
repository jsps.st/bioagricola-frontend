import Opcion1 from "../modules/modulo1/opcion1/opcion1";
import Opcion2 from "../modules/modulo1/opcion2/opcion2";
import NotFound from "../pages/notfound/notfound";

export const routes = [
    {
        'path':'/modulo1/opcion1',
        'component': Opcion1,
        'module':'modulo1',
        'title':'Opcion 1',
        'exact': true
    },
    {
        'path':'/modulo1/opcion2',
        'component':Opcion2,
        'title':'Opcion 2',
        'module':'modulo1'
    },
    {
        'path':'*',
        'component': NotFound
    }
]
