import * as React from "react";
import { Switch, Route } from 'react-router-dom'
import { routes } from '../../routes/routes-config'
import NotFound from "../../pages/notfound/notfound";

class Container extends React.Component {
    render() {
        return (
            <div class="container-fluid">
                <Switch>
                    {/*The routes array is used here and is iterated through to build the different routes needed for the app*/}
                    {routes.map((route,index) => (
                        <Route key={index} path={route.path} component={route.component} exact={route.exact} />
                    ))}
                    <Route component={NotFound}/>
                </Switch>
            </div>
        );
    }
}

export default Container;
