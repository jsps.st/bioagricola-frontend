import * as React from "react";
import Sidebar from "../sidebar/sidebar";
import Container from "../container/container";
import {BrowserRouter} from "react-router-dom";

class Main extends React.Component {
    render() {
        return (
            <div className="d-flex" id="wrapper">
                <BrowserRouter>
                    <Sidebar/>
                    <Container/>
                </BrowserRouter>
            </div>
        );
    }
}

export default Main;
