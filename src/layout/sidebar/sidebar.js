import * as React from "react";
import { NavLink } from 'react-router-dom'
import './sidebar.css';
import { routes } from '../../routes/routes-config'
class Sidebar extends React.Component {
    render() {
        return (
            <div class="bg-light border-right" id="sidebar-wrapper">
                <div class="sidebar-heading">Menu Opciones </div>
                <div class="list-group list-group-flush">
                    {routes.map((route,index) => (
                        <NavLink  key={index} to={route.path} className="list-group-item list-group-item-action " activeClassName="active">
                            {route.title}
                        </NavLink>
                    ))}
                </div>
            </div>
        );
    }
}
export default  Sidebar;
