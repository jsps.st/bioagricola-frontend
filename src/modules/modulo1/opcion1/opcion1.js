import * as React from "react";
import SuscripcionInfo from "../../../components/shared/suscripcion-info/suscripcion-info";


class Opcion1 extends React.Component {
    constructor (props){
        super(props);
        this.state= {suscripcion: {'id':'1','nombre':'Jimmy Pardo','servicio':'Gas'}}
    }
    render() {
        return (
            <div>
                  <SuscripcionInfo suscripcion={this.state.suscripcion}/>
            </div>
        );
    }
}

export default Opcion1;
