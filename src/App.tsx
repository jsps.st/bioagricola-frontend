import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import {SuscripcionInfo} from "./components/suscripcion/suscripcion-info";
import {SuscripcionModel} from "./models/suscripcion";

type AppProps = {suscripcion : SuscripcionModel}
export class App extends Component<AppProps>{
  render(){
          return (
              <div className="App">
                <header className="App-header">
                  <img src={logo} className="App-logo" alt="logo" />
                  <p>
                    Edit <code>src/App.tsx</code> and save to reload.
                  </p>
                </header>
                <SuscripcionInfo suscripcion={this.props.suscripcion} label="asds"/>
              </div>
          )
  }
}


export default App;
