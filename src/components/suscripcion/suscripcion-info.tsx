import * as React from "react";
import {SuscripcionModel} from "../../models/suscripcion";

export interface SuscripcionInfoProps {
    suscripcion: SuscripcionModel;  label: string;
}

export class SuscripcionInfo extends React.Component<SuscripcionInfoProps>{
    render() {
        return <div>{this.props.suscripcion.id}</div>;
    }
}
